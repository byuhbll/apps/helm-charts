# What Is This Project #

This project produces a helm repository hosted on gitlab pages. For more information on helm and helm repositories, 
visit https://helm.sh/.

## Getting Started ##

To use the charts in this project, you will need a working helm installation. Once you have that, you can simply run
`helm repo add byuhbll https://byuhbll.gitlab.io/apps/helm-charts` to add the chart repository to your helm 
installation.

## Provided Charts ##

This project provides several helm charts. Their names and descriptions are provided below.

#### spring-app ####

This chart is designed to run a spring application. Of particular note is the fact that application traffic is routed
to port 8080, but healthchecks are performed on port 8081.

## Updating These Charts ##

Because of the way gitlab pages works, only the charts that are currently in the master branch will be part of the
helm repository. It is also not an easy task to version charts. Due to these limitations, once a chart is added, it
should never be removed, nor should backwards compatibility be broken. If you need to introduce a breaking change
to a chart, you should copy the chart, version the name (such as `spring-app-2`), and then make your changes in the
new chart. Minor changes that do not break backwards compatibility can be made in place.