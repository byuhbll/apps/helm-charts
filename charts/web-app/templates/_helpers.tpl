{{- define "hostname" }}
{{- $hostname := printf "%s" .Values.route.url | trimPrefix "https://" }}
{{- regexSplit "/" $hostname -1 | first }}
{{- end }}

{{- define "contextRoot" }}
{{- printf "%s" .Values.route.url | trimPrefix "https://" | trimPrefix (include "hostname" .) }}
{{- end }}