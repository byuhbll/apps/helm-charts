
{{- define "appname" -}}
{{- $appname := printf "%s-%s" .Values.gitlabEnvironmentSlug .Values.application.track -}}
{{- $appname | trimSuffix "-stable" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "hostname" -}}
{{- $hostname := printf "%s" .Values.gitlabEnvironmentUrl | trimPrefix "https://" -}}
{{- regexSplit "/" $hostname -1 | first -}}
{{- end -}}

{{- define "contextRoot" -}}
{{- printf "%s" .Values.gitlabEnvironmentUrl | trimPrefix "https://" | trimPrefix (include "hostname" .) -}}
{{- end -}}